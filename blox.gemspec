# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'blox/version'

Gem::Specification.new do |spec|
  spec.name          = "blox"
  spec.version       = Blox::VERSION
  spec.authors       = ["Alexander Andersson"]
  spec.email         = ["alex.andersson@mittmedia.se"]
  spec.summary       = "MittMedia React component library"
  spec.homepage      = "https://bitbucket.org/mrandersson/blox"
  spec.license       = "MIT"

  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.files         = Dir["{lib,src}/**/*"]

  spec.add_dependency "railties", "~> 4.1"

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
end
