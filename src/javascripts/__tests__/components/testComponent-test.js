jest.dontMock('../../components/testComponent');

describe('TestComponent', function() {
  var React = require('react/addons');
  var TestUtils = React.addons.TestUtils;
  var TestComponent = require('../../components/testComponent');
  
  it('renders', function() {
    testComponent = TestUtils.renderIntoDocument(<TestComponent />)
    expect(testComponent.refs.editor).toBeDefined()
  });
});