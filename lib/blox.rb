require "blox/version"

module Blox
  module Rails
    class Engine < ::Rails::Engine
      config.assets.paths << File.expand_path("../../src/javascripts/components", __FILE__)
    end
  end
end  